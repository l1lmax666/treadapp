﻿namespace ThreadApp
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.twFiles = new System.Windows.Forms.TreeView();
            this.labTime = new System.Windows.Forms.Label();
            this.bPath = new System.Windows.Forms.Button();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.bCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.bStop = new System.Windows.Forms.Button();
            this.cbFileType = new System.Windows.Forms.ComboBox();
            this.bSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rtbFileContent = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labStatus = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // twFiles
            // 
            this.twFiles.Location = new System.Drawing.Point(14, 9);
            this.twFiles.Name = "twFiles";
            this.twFiles.Size = new System.Drawing.Size(356, 510);
            this.twFiles.TabIndex = 16;
            this.twFiles.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.twFiles_AfterSelect);
            // 
            // labTime
            // 
            this.labTime.AutoSize = true;
            this.labTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labTime.Location = new System.Drawing.Point(6, 118);
            this.labTime.Name = "labTime";
            this.labTime.Size = new System.Drawing.Size(156, 17);
            this.labTime.TabIndex = 12;
            this.labTime.Text = "Время поиска: 0:00:00";
            // 
            // bPath
            // 
            this.bPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.bPath.Location = new System.Drawing.Point(6, 22);
            this.bPath.Name = "bPath";
            this.bPath.Size = new System.Drawing.Size(608, 23);
            this.bPath.TabIndex = 3;
            this.bPath.Text = "Выбрать путь";
            this.bPath.UseVisualStyleBackColor = true;
            this.bPath.Click += new System.EventHandler(this.bPath_Click);
            // 
            // tbFileName
            // 
            this.tbFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tbFileName.Location = new System.Drawing.Point(6, 82);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(443, 20);
            this.tbFileName.TabIndex = 7;
            // 
            // bCancel
            // 
            this.bCancel.Enabled = false;
            this.bCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.bCancel.Location = new System.Drawing.Point(470, 115);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(144, 23);
            this.bCancel.TabIndex = 11;
            this.bCancel.Text = "Отменить поиск";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.Location = new System.Drawing.Point(452, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Тип файла";
            // 
            // bStop
            // 
            this.bStop.Enabled = false;
            this.bStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.bStop.Location = new System.Drawing.Point(326, 115);
            this.bStop.Name = "bStop";
            this.bStop.Size = new System.Drawing.Size(144, 23);
            this.bStop.TabIndex = 10;
            this.bStop.Text = "Приостановить поиск";
            this.bStop.UseVisualStyleBackColor = true;
            this.bStop.Click += new System.EventHandler(this.bStop_Click);
            // 
            // cbFileType
            // 
            this.cbFileType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cbFileType.FormattingEnabled = true;
            this.cbFileType.Location = new System.Drawing.Point(455, 81);
            this.cbFileType.Name = "cbFileType";
            this.cbFileType.Size = new System.Drawing.Size(159, 21);
            this.cbFileType.TabIndex = 6;
            // 
            // bSearch
            // 
            this.bSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.bSearch.Location = new System.Drawing.Point(182, 115);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(144, 23);
            this.bSearch.TabIndex = 9;
            this.bSearch.Text = "Начать поиск";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.Location = new System.Drawing.Point(6, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Имя файла";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rtbFileContent);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.groupBox2.Location = new System.Drawing.Point(376, 162);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(620, 357);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Содержимое файла";
            // 
            // rtbFileContent
            // 
            this.rtbFileContent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.rtbFileContent.Location = new System.Drawing.Point(6, 22);
            this.rtbFileContent.Name = "rtbFileContent";
            this.rtbFileContent.Size = new System.Drawing.Size(608, 329);
            this.rtbFileContent.TabIndex = 0;
            this.rtbFileContent.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labTime);
            this.groupBox1.Controls.Add(this.bPath);
            this.groupBox1.Controls.Add(this.tbFileName);
            this.groupBox1.Controls.Add(this.bCancel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.bStop);
            this.groupBox1.Controls.Add(this.cbFileType);
            this.groupBox1.Controls.Add(this.bSearch);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.groupBox1.Location = new System.Drawing.Point(376, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(620, 147);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры поиска";
            // 
            // labStatus
            // 
            this.labStatus.AutoSize = true;
            this.labStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.labStatus.Location = new System.Drawing.Point(11, 522);
            this.labStatus.Name = "labStatus";
            this.labStatus.Size = new System.Drawing.Size(138, 30);
            this.labStatus.TabIndex = 19;
            this.labStatus.Text = "Проверенно файлов: -\r\nТекущий файл: -";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 561);
            this.Controls.Add(this.twFiles);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labStatus);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView twFiles;
        private System.Windows.Forms.Label labTime;
        private System.Windows.Forms.Button bPath;
        private System.Windows.Forms.TextBox tbFileName;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bStop;
        private System.Windows.Forms.ComboBox cbFileType;
        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox rtbFileContent;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labStatus;
    }
}

