﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreadApp
{
    public partial class Form1 : Form
    {
        string FilePath = "";
        string FileName = "";
        string FileType = "";

        int CountFiles = 0;
        int CountFoundFiles = 0;

        DateTime Time = new DateTime();

        Thread TimerThread;
        Thread SearchThread;
        bool TimerWaitRun = true;
        bool SearchWaitRun = true;

        bool ThreadKill = true;
        bool ThreadStop = false;

        bool Complete = false;

        bool CancelClick = false;

        INI ini = new INI(Environment.CurrentDirectory + "\\save_state.ini");

        async Task ThreadPause()
        {
            while (ThreadStop)
            {
                if (ThreadKill) break;
                await Task.Delay(1);
            }
        }

        async Task ThreadRun()
        {
            while (ThreadKill)
            {
                await Task.Delay(1);
            }
        }

        void FillTypes(ComboBox comboBox)
        {
            comboBox.Items.Add(".txt");
            comboBox.SelectedIndex = 0;
        }

        void CompleteForm()
        {
            Complete = true;

            if (InvokeRequired)
            {
                Invoke((MethodInvoker)(() =>
                {
                    labStatus.Text = "Поиск завершен\nНайдено совпадений: " + CountFoundFiles;
                    bStop.Enabled = false;
                    bCancel.Enabled = false;
                    bPath.Enabled = true;
                    bSearch.Enabled = true;

                }));
            }
            else
            {
                labStatus.Text = "Поиск завершен\nНайдено совпадений: " + CountFoundFiles;

                bStop.Enabled = false;
                bCancel.Enabled = false;
                bPath.Enabled = true;
                bSearch.Enabled = true;
            }

            MessageBox.Show("Найдено совпадений: " + CountFoundFiles, "Поиск завершен");
        }

        void ClearForm()
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)(() =>
                {
                    twFiles.Nodes.Clear();
                    rtbFileContent.Clear();
                    labStatus.Text = "Проверенно файлов: -\nТекущий файл: -";
                }));
            }
            else
            {
                twFiles.Nodes.Clear();
                rtbFileContent.Clear();
                labStatus.Text = "Проверенно файлов: -\nТекущий файл: -";
            }
        }

        void LoadState()
        {
            string path = ini.GetPrivateString("Main", "FilePath");
            string name = ini.GetPrivateString("Main", "FileName");
            string type = ini.GetPrivateString("Main", "FileType");

            if (!String.IsNullOrEmpty(path))
            {
                FilePath = path;
                bPath.Text = "Выбранный путь: " + FilePath;
            }

            if (!String.IsNullOrEmpty(name))
            {
                FileName = name;
                tbFileName.Text = FileName;
            }

            if (!String.IsNullOrEmpty(type))
            {
                cbFileType.SelectedIndex = int.Parse(type);
            }
        }

        public Form1()
        {
            InitializeComponent();

            bStop.Enabled = false;
            bCancel.Enabled = false;

            FillTypes(cbFileType);

            LoadState();

            TimerThread = new Thread(async () =>
            {
                void PrintTime(int i, Label label)
                {
                    try
                    {
                        if (InvokeRequired)
                        {
                            Invoke((MethodInvoker)(() =>
                            {
                                label.Text = "Время поиска: " + Time.AddSeconds(i).ToLongTimeString();
                            }));
                        }
                        else
                        {
                            label.Text = "Время поиска: " + Time.AddSeconds(i).ToLongTimeString();
                        }
                    }
                    catch(ObjectDisposedException) { }
                }

                int tick = 0;

                while (true)
                {
                    if (!ThreadKill)
                    {
                        PrintTime(tick++, labTime);

                        await Task.Delay(1000);

                        await ThreadPause();
                    }
                    else
                    {
                        tick = 0;

                        if (!Complete)
                        {
                            PrintTime(tick, labTime);
                        }

                        TimerWaitRun = true;

                        await ThreadRun();
                    }
                }
            });

            SearchThread = new Thread(async () =>
            {
                async Task DirSearch(string Dir, string FileName, string FileType, Label Status)
                {
                    CountFiles = 0;
                    CountFoundFiles = 0;

                    foreach (string file in Directory.GetFiles(Dir))
                    {
                        try
                        {
                            if (ThreadKill) break;

                            await ThreadPause();

                            try
                            { 
                                CountFiles++;

                                if (InvokeRequired)
                                {
                                    Invoke((MethodInvoker)(() =>
                                    {
                                        Status.Text = "Проверенно файлов: " + CountFiles + "\nТекущий файл: " + file;
                                    }));
                                }
                                else Status.Text = "Проверенно файлов: " + CountFiles + "\nТекущий файл: " + file;

                                if (!String.IsNullOrEmpty(FileName))
                                {
                                    if (file == Dir + "\\" + FileName + FileType)
                                    {
                                        CountFoundFiles++;

                                        if (InvokeRequired)
                                        {
                                            Invoke((MethodInvoker)(() =>
                                            {
                                                FillFilesTree(twFiles, file);
                                            }));
                                        }
                                        else FillFilesTree(twFiles, file);
                                    }
                                }
                                else
                                {
                                    if (file.Length >= FileType.Length)
                                    {
                                        if (file.Substring(file.Length - 4, 4) == FileType)
                                        {
                                            CountFoundFiles++;

                                            if (InvokeRequired)
                                            {
                                                Invoke((MethodInvoker)(() =>
                                                {
                                                    FillFilesTree(twFiles, file);
                                                }));
                                            }
                                            else FillFilesTree(twFiles, file);
                                        }
                                    }
                                }

                                await Task.Delay(1);
                            }
                            catch (ObjectDisposedException) { }
                        }
                        catch (UnauthorizedAccessException) { }
                    }

                    await NestedDirSearch(Dir, FileName, FileType, Status);
                }

                async Task NestedDirSearch(string Dir, string FileName, string FileType, Label Status)
                {
                    foreach (string dir in Directory.GetDirectories(Dir))
                    {
                        try
                        {
                            if (ThreadKill) break;

                            await ThreadPause();

                            foreach (string file in Directory.GetFiles(dir))
                            {
                                try
                                {
                                    if (ThreadKill) break;

                                    await ThreadPause();

                                    try
                                    {
                                        CountFiles++;

                                        if (InvokeRequired)
                                        {
                                            Invoke((MethodInvoker)(() =>
                                            {
                                                Status.Text = "Проверенно файлов: " + CountFiles + "\nТекущий файл: " + file;
                                            }));
                                        }
                                        else Status.Text = "Проверенно файлов: " + CountFiles + "\nТекущий файл: " + file;

                                        if (!String.IsNullOrEmpty(FileName))
                                        {
                                            if (file == dir + "\\" + FileName + FileType)
                                            {
                                                CountFoundFiles++;

                                                if (InvokeRequired)
                                                {
                                                    Invoke((MethodInvoker)(() =>
                                                    {
                                                        FillFilesTree(twFiles, file);
                                                    }));
                                                }
                                                else FillFilesTree(twFiles, file);
                                            }
                                        }
                                        else
                                        {
                                            if (file.Length >= FileType.Length)
                                            {
                                                if (file.Substring(file.Length - 4, 4) == FileType)
                                                {
                                                    CountFoundFiles++;

                                                    if (InvokeRequired)
                                                    {
                                                        Invoke((MethodInvoker)(() =>
                                                        {
                                                            FillFilesTree(twFiles, file);
                                                        }));
                                                    }
                                                    else FillFilesTree(twFiles, file);
                                                }
                                            }
                                        }

                                        await Task.Delay(1);
                                    }
                                    catch (ObjectDisposedException) { }
                                }
                                catch (UnauthorizedAccessException) { }
                            }

                            await NestedDirSearch(dir, FileName, FileType, Status); 
                        }
                        catch (UnauthorizedAccessException) { }
                    }
                }

                void FillFilesTree(TreeView treeView, string Path)
                {
                    TreeNodeCollection nodes = treeView.Nodes;

                    foreach (string PathPart in Path.Split('\\'))
                    {
                        if (!nodes.ContainsKey(PathPart))
                        {
                            nodes.Add(PathPart, PathPart);
                        }

                        nodes = nodes[PathPart].Nodes;
                    }

                    treeView.ExpandAll();
                }

                while (true)
                {
                    if (!ThreadKill) //Если поток не убит, запустить таймер и поиск
                    {
                        await DirSearch(FilePath, FileName, FileType, labStatus);

                        ThreadKill = true;
                        ThreadStop = false;

                        if (!CancelClick)
                        {
                            CompleteForm();
                        }
                    }
                    else
                    {
                        if (!Complete)
                        {
                            ClearForm();
                        }

                        SearchWaitRun = true;

                        await ThreadRun();
                    }
                }
            });

            TimerThread.Start();
            SearchThread.Start();
        }


        void bPath_Click(object sender, EventArgs e)
        {
            using (var folderBrowserDialog = new FolderBrowserDialog())
            {
                DialogResult Result = folderBrowserDialog.ShowDialog();

                if (Result == DialogResult.OK && !string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath))
                {
                    FilePath = folderBrowserDialog.SelectedPath.ToString();
                }
            }

            if (!String.IsNullOrEmpty(FilePath))
            {
                bPath.Text = "Выбранный путь: " + FilePath;
            }
            else
            {
                bPath.Text = "Выбрать путь";
            }
        }

        void bSearch_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(FilePath) && cbFileType.SelectedIndex > -1)
            {
                CancelClick = false;

                bSearch.Enabled = false;
                bPath.Enabled = false;

                if (!String.IsNullOrEmpty(tbFileName.Text)) FileName = tbFileName.Text;
                else FileName = "";

                FileType = cbFileType.SelectedItem.ToString();

                ClearForm();

                Complete = false;
                ThreadKill = false;
                ThreadStop = false;

                bStop.Enabled = true;
                bCancel.Enabled = true;
            }
            else
            {
                labStatus.Text = "\nЗаполните все параметры поиска";
            }
        }

        void bStop_Click(object sender, EventArgs e)
        {
            if (ThreadStop)
            {
                ThreadStop = false;
                bStop.Text = "Приостановить поиск";
            }
            else
            {
                ThreadStop = true;
                bStop.Text = "Возобновить поиск";
            }
        }

        void bCancel_Click(object sender, EventArgs e)
        {
            CancelClick = true;

            bCancel.Enabled = false;
            bStop.Enabled = false;

            Complete = false;
            ThreadKill = true;
            ThreadStop = false;

            bStop.Text = "Приостановить поиск";

            while (true)
            {
                if (TimerWaitRun && SearchWaitRun)
                {
                    if (InvokeRequired)
                    {
                        Invoke((MethodInvoker)(() =>
                        {
                            bSearch.Enabled = true;
                            bPath.Enabled = true;
                        }));
                    }
                    else
                    {
                        bSearch.Enabled = true;
                        bPath.Enabled = true;
                    }

                    break;
                }
            }
        }

        void twFiles_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode CurrentNode = e.Node;
            string path = CurrentNode.FullPath;
            
            rtbFileContent.Clear();

            if (path.Length >= FileType.Length)
            {
                if (path.Substring(path.Length - 4, 4) == FileType)
                {
                    using (StreamReader streamReader = new StreamReader(path))
                    {   
                        while (true)
                        {
                            string temp = streamReader.ReadLine();

                            if (temp == null) break;
                            
                            rtbFileContent.Text += temp + "\n";
                        }
                    }
                }
            }
        }

        void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            ini.WritePrivateString("Main", "FilePath", FilePath);
            ini.WritePrivateString("Main", "FileName", tbFileName.Text);
            ini.WritePrivateString("Main", "FileType", cbFileType.SelectedIndex.ToString());
        }
    }
}
